export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavTitle',
        _children: ['จองห้องประชุม']
      },
      {
        _name: 'CSidebarNavItem',
        name: 'ค้นหาห้องว่าง',
        to: '/',
        icon: 'cil-zoom'
      },
      {
        _name: 'CSidebarNavItem',
        name: 'ตารางการใช้ห้อง',
        to: '/',
        icon: 'cil-calendar'
      },
      {
        _name: 'CSidebarNavTitle',
        _children: ['สำหรับ Admin']
      },
      {
        _name: 'CSidebarNavItem',
        name: 'จัดการห้อง',
        to: '/',
        icon: 'cil-room'
      },
      {
        _name: 'CSidebarNavItem',
        name: 'จัดการ Location',
        to: '/',
        icon: 'cil-location-pin'
      },
    ]
  }
]